package ru.edu.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.edu.dao.CityRepository;

import java.util.ArrayList;
import java.util.List;

@Component
public class CityService {

    private CityRepository repository;

    @Autowired
    public void setRepository(CityRepository repository) {
        this.repository = repository;
    }

    /**
     * Get all.
     *
     * @author -  Меньщиков Константин
     */
    public List<CityInfo> getAll() {
        return new ArrayList<>();
    }

    /**
     * Get city by id. Returns null if not found.
     *
     * @param cityId - city id
     * @throws ru.edu.error.CustomException with code CITY_NOT_FOUND if city doesn't exist
     * @author - Мелёхин Алексей
     */
    public CityInfo getCity(String cityId) {
        return null;
    }

    /**
     * Create new city.
     *
     * @throws ru.edu.error.CustomException with code CITY_ALREADY_EXISTS if city with current id already exists
     * @throws ru.edu.error.CustomException with code ARGUMENT_ERROR if info is incorrect
     * @author - Истомин Михаил
     */
    public CityInfo create(CityInfo info) {
        return null;
    }

    /**
     * Update existing city. Don't change id
     *
     * @throws ru.edu.error.CustomException with code CITY_NOT_FOUND if city doesn't exist
     * @throws ru.edu.error.CustomException with code ARGUMENT_ERROR if info is incorrect
     * @author - Насибуллина Гульназ
     */
    public CityInfo update(CityInfo info) {
        return null;
    }

    /**
     * Delete city by id
     *
     * @throws ru.edu.error.CustomException with code CITY_NOT_FOUND if city doesn't exist
     * @author - Истомин Михаил
     */
    public CityInfo delete(String cityId) {
        return null;
    }

    /**
     * Get city weather
     *
     * @throws ru.edu.error.CustomException with code CITY_NOT_FOUND if city doesn't exist
     * @author - Истомин Михаил
     */
    public String getWeather(String cityId) {
        return "+15";
    }

    /**
     * Get city distance
     *
     * @throws ru.edu.error.CustomException with code CITY_NOT_FOUND if city doesn't exist
     * @author - Истомин Михаил
     */
    public String getDistance(String fromCityId, String toCityId) {
        return "124km";
    }
}
