package ru.edu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import ru.edu.service.CityInfo;
import ru.edu.service.CityService;

import java.util.ArrayList;
import java.util.List;

@RestController
public class CityRestController {

    private CityService cityService;

    @Autowired
    public void setCityService(CityService cityService) {
        this.cityService = cityService;
    }

    /**
     * Get all.
     *
     * @author -  Меньщиков Константин
     */
    public List<CityInfo> getAll() {
        return new ArrayList<>();
    }

    /**
     * Get city by id. Returns null if not found.
     *
     * @param cityId - item id
     * @author - Мелёхин Алексей
     */
    public CityInfo getCity(String cityId) {
        return null;
    }

    /**
     * Create new city.
     *
     * @author - Истомин Михаил
     */
    public CityInfo create(CityInfo info) {
        return null;
    }

    /**
     * Update existing city. Don't change id
     *
     * @author - Насибуллина Гульназ
     */
    public CityInfo update(CityInfo info) {
        return null;
    }

    /**
     * Delete city by id.
     *
     * @author - Истомин Михаил
     */
    public CityInfo delete(String cityId) {
        return null;
    }
}
