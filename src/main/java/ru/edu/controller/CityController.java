package ru.edu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.edu.service.CityInfo;
import ru.edu.service.CityService;

@Controller
@RequestMapping("/travel")
public class CityController {

    private CityService cityService;

    @Autowired
    public void setCityService(CityService cityService) {
        this.cityService = cityService;
    }

    /**
     * Get all cities view.
     *
     * @author -  Меньщиков Константин
     */
    @GetMapping
    public ModelAndView getAllCitiesView() {
        return null;
    }

    /**
     * Get city view.
     *
     * @param cityId - city id
     * @author - Мелёхин Алексей
     */
    @GetMapping("/city")
    public ModelAndView getCityView(@RequestParam("cityId") String cityId) {
        return null;
    }

    /**
     * Get create new city view.
     *
     * @author - Насибуллина Гульназ
     */
    @GetMapping("/city/create")
    public ModelAndView getCreateCityView() {
        return null;
    }

    /**
     * Create new city.
     *
     * @author - Насибуллина Гульназ
     */
    @PostMapping("/city/create")
    public ModelAndView createCity(@RequestBody CityInfo info) {
        return null;
    }

    /**
     * Get delete city view.
     *
     * @author - Истомин Михаил
     */
    @GetMapping("/city/delete")
    public ModelAndView getDeleteCityView() {
        return null;
    }

    /**
     * Delete city.
     *
     * @author - Истомин Михаил
     */
    @PostMapping("/city/delete")
    public ModelAndView deleteCity(@RequestParam("cityId") String cityId) {
        return null;
    }
}
